import UIKit


@IBDesignable

class loginTextField: UITextField{
    
    override func prepareForInterfaceBuilder() {
        customizeView()
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        customizeView()
    }
    
    func customizeView(){
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textAlignment = .left
        
        layer.cornerRadius = 0.0
        layer.borderWidth = 22.0
        layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        
        if let p = placeholder{
            let place = NSAttributedString(string: p, attributes: [.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
            attributedPlaceholder = place
            textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
}
