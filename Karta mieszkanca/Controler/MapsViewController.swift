import UIKit
import MapKit

class MapsViewController: UIViewController {

    @IBOutlet weak var mapsView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let locations = [
            Location(localizationId : 1, localizationName: "Nazwa miasta 1", localizationDescription : "Opis miasta 1", localizationPhoto : "http://www.nazwa.pl/foto.png", latitude: 54.570783, longitude: 18.387822),
            Location(localizationId : 2, localizationName: "Nazwa miasta 2", localizationDescription : "Opis miasta 2", localizationPhoto : "http://www.nazwa.pl/foto.png", latitude: 54.441581, longitude: 54.441581),
            Location(localizationId : 3, localizationName: "Nazwa miasta 3", localizationDescription : "Opis miasta 3", localizationPhoto : "http://www.nazwa.pl/foto.png", latitude: 54.352025, longitude: 18.646638)
        ]
        
        let annotations = locations.map { location -> MKAnnotation in
            let annotation = MKPointAnnotation()
            annotation.title = location.localizationName
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            return annotation
        }
        mapsView.addAnnotations(annotations)
        
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
