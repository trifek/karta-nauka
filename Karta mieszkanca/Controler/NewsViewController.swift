import UIKit

class NewsViewController: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Sprawdzam obiekt: \(AppGlobalManager.sharedManager.loggedUser)")
        dump(AppGlobalManager.sharedManager.loggedUser)
    }
    
    @IBAction func testBtnPressed(_ sender: Any) {
        print("Sprawdzam obiekt: \(AppGlobalManager.sharedManager.loggedUser)")
        dump(AppGlobalManager.sharedManager.loggedUser)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Aktualności"
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
