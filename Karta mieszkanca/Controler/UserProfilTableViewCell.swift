//
//  UserProfilTableViewCell.swift
//  Karta mieszkanca
//
//  Created by iMac on 11.05.2018.
//  Copyright © 2018 Łukasz Peta. All rights reserved.
//



import UIKit

protocol showAlertMessageProtocol {
    func showMessage(txt: String, title: String)
}

class UserProfilTableViewCell: UITableViewCell {
    
    // outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var peselLabel: UILabel!
    @IBOutlet weak var cultureSwitch: UISwitch!
    @IBOutlet weak var sportSwitch: UISwitch!
    @IBOutlet weak var personalSwitch: UISwitch!
    @IBOutlet weak var budgetSwitch: UISwitch!
    @IBOutlet weak var healthSwitch: UISwitch!
    @IBOutlet weak var gardenSwitch: UISwitch! 
    @IBOutlet weak var mailAcceptSwitch: UISwitch!
    @IBOutlet weak var streetTxtField: UITextField!
    @IBOutlet weak var streetNumberTxtField: UITextField!
    @IBOutlet weak var streetLocalNumberTxtField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var postCodeTxtField: UITextField!
    @IBOutlet weak var districtTxtField: UITextField!
    @IBOutlet weak var pinTxtField: UITextField!
    @IBOutlet weak var email2TxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var password1TxtField: UITextField!
    @IBOutlet weak var password2TxtField: UITextField!
    
    var delegate: showAlertMessageProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadUserProfileData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

// MARK: Update UI
extension UserProfilTableViewCell {
    func loadUserProfileData(){
        nameLabel?.text = AppGlobalManager.sharedManager.loggedUser?.imie
        surnameLabel?.text = AppGlobalManager.sharedManager.loggedUser?.nazwisko
        peselLabel?.text = AppGlobalManager.sharedManager.loggedUser?.id
        
        streetTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.ulica
        streetNumberTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.nrDomu
        streetLocalNumberTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.nrLokalu
        cityTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.miasto
        postCodeTxtField?.text = nil
        districtTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.dzielnica
        pinTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.pin
        email2TxtField?.text = AppGlobalManager.sharedManager.loggedUser?.email
        emailTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.email
        phoneTxtField?.text = AppGlobalManager.sharedManager.loggedUser?.telefon
        password1TxtField?.text = nil
        password2TxtField?.text = nil
        
        for zainteresowania in  (AppGlobalManager.sharedManager.loggedUser?.zainteresowania)!{
            print("\(zainteresowania)")
        }
        
        
        
        //        cultureSwitch.isOn
        //        sportSwitch.isOn
        //        personalSwitch.isOn
        //        budgetSwitch.isOn
        //        healthSwitch.isOn
        //        gardenSwitch.isOn
        //        mailAcceptSwitch.isOn
    }
}

// MARK: - User Interaction
extension UserProfilTableViewCell {
    @IBAction func saveBtnPressed(_ sender: Any) {
        if password1TxtField.text != "" || password2TxtField.text != ""  {
            changeUserPassword(newPassword1: password1TxtField.text!, newPassword2: password2TxtField.text!)
        }
        if emailTxtField.text != "" || email2TxtField.text != ""  {
            changeUserEmail(email1: emailTxtField.text!, email2: email2TxtField.text!)
        }
        
        changeUserData(street: streetTxtField.text, streetNumber: streetNumberTxtField.text, streetLocalNumber: streetLocalNumberTxtField.text, city: cityTxtField.text, postCode: postCodeTxtField.text, district: districtTxtField.text, pin: pinTxtField.text, phone: phoneTxtField.text)
        
    }
    
    func changeUserData(street: String?, streetNumber: String?, streetLocalNumber: String?, city: String?, postCode: String?, district: String?, pin: String?, phone: String?){
        let cms = ServerConnect()
        
        cms.saveUserData(street: street, streetNumber: streetNumber, streetLocalNumber: streetLocalNumber, city: city, postCode: postCode, district: district, pin: pin, phone: phone, completion: { (data) in
            
            switch(data) {
            case .succes(let data):
                var actionToRun: ()-> Void
                
                if let json = try? JSONSerialization.jsonObject(with: data, options: []),
                    let dictionary = json as? [String: Any],
                    let message = dictionary["komunikat"] as? String,
                    let title = dictionary["error"] as? String {
                    // we have an error
                    actionToRun = {
                        print("Dane użytkownika zostały zmienione")
                    }
                }
                else {
                    actionToRun = {
                        print("Nastąpił problem z zapisem danych użytkownika")
                        self.delegate.showMessage(txt: "Nastąpił problem z zapisem danych użytkownika", title: "Informacja")
                    }
                }
                DispatchQueue.main.async {
                    actionToRun()
                }
                
            case .error(let error):
                print("Error 107: \(error)")
            }
            
        })
    }
    
    
    func changeUserEmail(email1: String, email2: String){
        if email1 != "" || email2 != "" {
            if email1 == email2 {
                print("Zmieniam email")
                
                let cms = ServerConnect()
                cms.saveUserEmail(email: email1, completion: { (data) in
                    
                    switch(data) {
                    case .succes(let data):
                        var actionToRun: ()-> Void
                        
                        if let json = try? JSONSerialization.jsonObject(with: data, options: []),
                            let dictionary = json as? [String: Any],
                            let message = dictionary["komunikat"] as? String,
                            let title = dictionary["error"] as? String {
                            // we have an error
                            actionToRun = {
                                print("Podany email został zmieniony")
                            }
                        }
                        else {
                            actionToRun = {
                                print("Nastąpił problem ze zmianą emaila")
                            }
                        }
                        DispatchQueue.main.async {
                            actionToRun()
                        }
                        
                    case .error(let error):
                        print("Error 106: \(error)")
                    }
                    
                })
                
            } else {
                print("Podane emaile różnią się od siebie")
            }
        } else {
            print("Proszę uzupełnić oba pola email")
        }
    }
    
    
    func changeUserPassword(newPassword1: String, newPassword2: String){
        if newPassword1 != "" || newPassword2 != "" {
            if newPassword1 == newPassword2 {
                if (newPassword1.count) >= 8{
                    print("Zmieniam hasło")
                    
                    let cms = ServerConnect()
                    cms.saveUserNewPassword(password: newPassword1, completion: { (data) in
                        
                        switch(data) {
                        case .succes(let data):
                            var actionToRun: ()-> Void
                            
                            if let json = try? JSONSerialization.jsonObject(with: data, options: []),
                                let dictionary = json as? [String: Any],
                                let message = dictionary["komunikat"] as? String,
                                let title = dictionary["error"] as? String {
                                // we have an error
                                actionToRun = {
                                    print("Podane hasło zostało zmienione")
                                }
                            }
                            else {
                                actionToRun = {
                                    print("Nastąpił problem ze zmianą hasła")
                                }
                            }
                            DispatchQueue.main.async {
                                actionToRun()
                            }
                            
                        case .error(let error):
                            print("Error 105: \(error)")
                        }
                        
                    })
                    
                } else {
                    print("Podane hasło jest za krótkie")
                }
            } else {
                print("Podane hasła różnią się od siebie")
            }
        } else{
            print("Proszę wypełnić poprawnie oba pola hasła")
        }
    }
    
    
}
