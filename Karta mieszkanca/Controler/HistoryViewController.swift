import UIKit

class HistoryViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Historia"
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
