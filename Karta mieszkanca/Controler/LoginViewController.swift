import UIKit

class LoginViewController: UIViewController {
    
    // outlets
    @IBOutlet weak var textFieldLogin: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        if self.textFieldLogin.text?.isEmpty ?? true || self.textFieldPassword.text?.isEmpty ?? true  {
            self.errorLoginMessage(txt: "Error", title: "Error")
        } else {
            let cms = ServerConnect()
            cms.checkUsersLogin(login: self.textFieldLogin.text, password: self.textFieldPassword.text, completion: { (data) in
                
                switch(data) {
                case .succes(let data):
                    var actionToRun: ()-> Void
                    var user : LoginUser?
                    let decoder = JSONDecoder()
                    user = try? decoder.decode(LoginUser.self, from: data)
                    dump(user)
                    
                    // we have an user
                    if ((user?.id ) != nil) {
                        actionToRun = {
                            AppGlobalManager.sharedManager.loggedUser = user
                            self.performSegue(withIdentifier: "toLoginUser", sender: self)
                        }
                    }
                        // we have an error
                    else if let json = try? JSONSerialization.jsonObject(with: data, options: []),
                        let dictionary = json as? [String: Any],
                        let message = dictionary["komunikat"] as? String,
                        let title = dictionary["error"] as? String {
                        // we have an error
                        actionToRun = {
                            self.errorLoginMessage(txt: message, title: title)
                        }
                    }
                        // default error
                    else {
                        actionToRun = {
                            self.errorLoginMessage(txt: "Podany login lub hasło jest błędny!!", title: "Błąd")
                        }
                    }
                    
                    DispatchQueue.main.async {
                        actionToRun()
                    }

                case .error(let error):
                    print("Error 104: \(error)")
                }
                
            })
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "toLoginUser" {
//            let tabVC = segue.destination as! UITabBarController
//            // assuming that `NewsViewController` is the first view controller in the tabbar controller:
//            let destinationViewController = tabVC.viewControllers?[0] as! NewsViewController
//            destinationViewController.loggedUser = self.user
            
        }
    }
    
    func errorLoginMessage(txt: String, title: String){
        let alertController = UIAlertController(title: title, message: txt, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

