import UIKit

class UserProfilViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, showAlertMessageProtocol {
    
    func showMessage(txt: String, title: String){
        let alertController = UIAlertController(title: title, message: txt, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // Outlets
    @IBOutlet weak var userProfileTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Profil"
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1092
    }
    
    
    
}

// MARK: - Table View Data Source
extension UserProfilViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfilCell", for: indexPath) as! UserProfilTableViewCell
        return cell
    }
}

