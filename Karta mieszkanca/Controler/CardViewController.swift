import UIKit

class CardViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Karta mieszkańca"
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
