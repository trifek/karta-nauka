import UIKit

class DiscountMainViewController: UIViewController {
    enum TabChangedValue: Int {
        case TabOne
        case TabTwo
    }
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var segmentPressed: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleChange(.TabOne)
    }
}

// MARK: Update UI
extension DiscountMainViewController {
    func handleChange(_ tab:TabChangedValue) {
        
        func hideAllViews() {
            [firstView, secondView].forEach { $0?.isHidden = true }
        }
        
        func showTab() {
            switch tab {
            case .TabOne: firstView.isHidden  = false
            case .TabTwo: secondView.isHidden = false
            }
        }
        
        hideAllViews()
        showTab()
    }
}

// MARK: - User Interaction
extension DiscountMainViewController {
    @IBAction func segmentSelectedAction(_ sender: Any) {
        guard let segmentChanged = TabChangedValue(rawValue: segmentPressed.selectedSegmentIndex) else {
            return
        }
        
        handleChange(segmentChanged)
    }
}
