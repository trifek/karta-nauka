import UIKit

class ProfileViewController: UIViewController {

   override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Profil"
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
