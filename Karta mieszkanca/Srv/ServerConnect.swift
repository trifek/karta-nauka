import Foundation

enum ApiConstans {
    static let BaseUrl = "http://serwer1356363.home.pl"
    static let iosPath = "/pub/ios/"
    static let jsonPath = "json.php"
    
    static var fullPath: String { return BaseUrl + iosPath + jsonPath }
}

struct ServerConnect {
    enum Result<T> {
        case succes(T)
        case error(String)
    }
    
    typealias completionHandler = (Result<Data >) -> ()
    
    func getJsonFromServer(parameters: String, completion: @escaping completionHandler) {
        let fullUrlString = ApiConstans.fullPath + parameters
        guard let url = URL(string: fullUrlString) else {
            debugPrint("Error 100: Problem with url")
            return completion(.error("Error 100: Problem with url"))
        }
        
        URLSession.shared.dataTask(with: url) {  (data, response, error) in
            guard error == nil else {
                debugPrint("Error 101: Problem with data")
                return completion(.error("Error 101: Problem with data"))
            }
            
            guard let data = data else {
                debugPrint("Error 102: Problem with data")
                return completion(.error("Error 102: Problem with data"))
            }
            
            debugPrint("R> \(fullUrlString)")
            return completion(.succes(data))
        }.resume()
    }
    
    func checkUsersLogin(login: String?, password: String?, completion: @escaping completionHandler) {
        self.getJsonFromServer(parameters: "?action=LOGOWANIE&login=\(login!)&password=\(password!)", completion: completion)
    }
    
    func saveUserNewPassword(password: String?, completion: @escaping completionHandler) {
        self.getJsonFromServer(parameters: "?action=XXXXXXXXXXX&password=\(password!)", completion: completion)
    }
    
    func saveUserEmail(email: String?, completion: @escaping completionHandler) {
        self.getJsonFromServer(parameters: "?action=XXXXXXXXXXX&email=\(email!)", completion: completion)
    }
    
    func saveUserData(street: String?, streetNumber: String?, streetLocalNumber: String?, city: String?, postCode: String?, district: String?, pin: String?, phone: String?, completion: @escaping completionHandler) {
        self.getJsonFromServer(parameters: "?action=XXXXXXXXXXX&street=\(street!)&streetNumber=\(streetNumber!)&streetLocalNumber=\(streetLocalNumber!)&city=\(city!)&postCode=\(postCode!)&district=\(district!)&pin=\(pin!)&phone=\(phone!)", completion: completion)
    }
    
}
