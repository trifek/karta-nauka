//
//  Locations.swift
//  Karta Mieszkańca
//
//  Created by iMac on 01.03.2018.
//  Copyright © 2018 iMac. All rights reserved.
//

import Foundation
import MapKit
// https://www.raywenderlich.com/160517/mapkit-tutorial-getting-started

class LocalizationModel: NSObject , MKAnnotation{
    let coordinate: CLLocationCoordinate2D
    let localizationId : Int?
    let localizationName : String
    let localizationDescription : String
    let localizationPhoto : String
    
    init(localizationId : Int, localizationName : String, localizationDescription : String, localizationPhoto : String, coordinate : CLLocationCoordinate2D) {
        self.localizationId = localizationId
        self.localizationName = localizationName
        self.localizationDescription = localizationDescription
        self.localizationPhoto = localizationPhoto
        self.coordinate = coordinate
    }
}

struct Location {
    let localizationId : Int?
    let localizationName : String
    let localizationDescription : String
    let localizationPhoto : String
    let latitude: Double
    let longitude: Double
}
