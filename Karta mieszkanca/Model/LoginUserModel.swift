//
//  UserModel.swift
//  Karta Mieszkańca
//
//  Created by iMac on 01.03.2018.
//  Copyright © 2018 iMac. All rights reserved.
//

import Foundation

struct LoginUser : Codable {
    let id : String?
    let imie : String?
    let nazwisko : String?
    let rodzina : [Rodzina]?
    let zainteresowania : [String]?
    let miasto : String?
    let ulica : String?
    let nrDomu : String?
    let nrLokalu : String?
    let dzielnica : String?
    let telefon : String?
    let email : String?
    let pin : String?
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        imie = try values.decodeIfPresent(String.self, forKey: .imie)
//        nazwisko = try values.decodeIfPresent(String.self, forKey: .nazwisko)
//        rodzina = try values.decodeIfPresent([Rodzina].self, forKey: .rodzina)
//        zainteresowania = try values.decodeIfPresent([String].self, forKey: .zainteresowania)
//        miasto = try values.decodeIfPresent(String.self, forKey: .miasto)
//        ulica = try values.decodeIfPresent(String.self, forKey: .ulica)
//        nrDomu = try values.decodeIfPresent(String.self, forKey: .nrDomu)
//        nrLokalu = try values.decodeIfPresent(String.self, forKey: .nrLokalu)
//        dzielnica = try values.decodeIfPresent(String.self, forKey: .dzielnica)
//        telefon = try values.decodeIfPresent(String.self, forKey: .telefon)
//        email = try values.decodeIfPresent(String.self, forKey: .email)
//        pin = try values.decodeIfPresent(String.self, forKey: .pin)
//    }
    
}



struct Rodzina : Codable {
    let id : String?
    let imie : String?
    let nazwisko : String?
    let rodzina : [String]?
    let zainteresowania : [String]?
    let miasto : String?
    let ulica : String?
    let nrDomu : String?
    let nrLokalu : String?
    let dzielnica : String?
    let telefon : String?
    let email : String?
    let pin : String?
    let imieNazwiskoOpiekuna : String?
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        imie = try values.decodeIfPresent(String.self, forKey: .imie)
//        nazwisko = try values.decodeIfPresent(String.self, forKey: .nazwisko)
//        rodzina = try values.decodeIfPresent([String].self, forKey: .rodzina)
//        zainteresowania = try values.decodeIfPresent([String].self, forKey: .zainteresowania)
//        miasto = try values.decodeIfPresent(String.self, forKey: .miasto)
//        ulica = try values.decodeIfPresent(String.self, forKey: .ulica)
//        nrDomu = try values.decodeIfPresent(String.self, forKey: .nrDomu)
//        nrLokalu = try values.decodeIfPresent(String.self, forKey: .nrLokalu)
//        dzielnica = try values.decodeIfPresent(String.self, forKey: .dzielnica)
//        telefon = try values.decodeIfPresent(String.self, forKey: .telefon)
//        email = try values.decodeIfPresent(String.self, forKey: .email)
//        pin = try values.decodeIfPresent(String.self, forKey: .pin)
//        imieNazwiskoOpiekuna = try values.decodeIfPresent(String.self, forKey: .imieNazwiskoOpiekuna)
//    }
    
}
