import Foundation

public struct Connect {
    let serverAdress = "http://nazwa.pl/"
    typealias Answer = (Data?, Error?) -> Void
    
    
    func getJsonFromServer(parameters: String, responseRequest: @escaping Answer) {
        guard let Url = URL(string: self.serverAdress + "kartyEndpoint.php" + parameters) else { return }
        URLSession.shared.dataTask(with: Url) { (data, response, error) in
            if error == nil {
                guard let data = data else {
                    print("Error 100:  \(error)")
                    responseRequest(nil, error)
                    return
                }
                print("R>" + self.serverAdress + "kartyEndpoint.php" + parameters)
                responseRequest(data, nil)
            } else{
                print("Error 101: Problem with download data")
            }
        }.resume()
    }
    
    func checkUsersLogin(login: String?, password: String?, callback: @escaping Answer) {
        getJsonFromServer(parameters: "?action=LOGOWANIE&login=\(login!)&password=\(password!)", responseRequest: callback)
    }
    
}
